nnoremap œ :q!<return>
inoremap h1<tab> <h1></h1><esc>4hi

nnoremap <space>v :vsp .<return>
nnoremap <space>h :sp .<return>

filetype plugin on
syntax on
